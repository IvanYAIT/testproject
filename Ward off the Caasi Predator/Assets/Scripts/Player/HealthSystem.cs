using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Player
{
    public class HealthSystem
    {
        private int _currentMaxHealth;
        private int _currentHealth;
        private List<Transform> _hearts;

        [Inject]
        public HealthSystem(PlayerCharacteristics playerCharacteristics, PlayerSettings playerSettings)
        {
            _currentMaxHealth = playerCharacteristics.Health;
            _currentHealth = playerCharacteristics.Health;
            _hearts = new List<Transform>();
            Transform heartsParent = playerSettings.HeartsParent;
            for (int i = 0; i < heartsParent.childCount; i++)
                _hearts.Add(heartsParent.GetChild(i));
            for (int i = 0; i < _currentMaxHealth; i++)
                _hearts[i].gameObject.SetActive(true);
        }

        public void GetDamage()
        {
            if (_currentHealth != 0)
            {
                _currentHealth--;
                _hearts[_currentHealth].gameObject.SetActive(false);
            }
            CheckHealth();
        }

        public bool HealHeart()
        {
            if (_currentMaxHealth != _currentHealth)
            {
                _hearts[_currentHealth].gameObject.SetActive(true);
                _currentHealth++;
                return true;
            }
            return false;
        }

        public void IncreaseMaxHealth()
        {
            if (_currentMaxHealth != _hearts.Count)
            {
                _currentMaxHealth++;
            }
        }

        public void DecreaseMaxHealth()
        {
            if (_currentMaxHealth != 1)
            {
                _currentMaxHealth--;
                if (_currentMaxHealth < _currentHealth)
                {
                    _currentHealth--;
                    _hearts[_currentHealth].gameObject.SetActive(false);
                }
            }
        }

        private void CheckHealth()
        {
            if (_currentHealth <= 0)
                Game.OnEnd?.Invoke();
        }
    }
}