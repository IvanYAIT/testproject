using UnityEngine;
using TMPro;

namespace Player
{
    public class BombView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI bombText;
        public TextMeshProUGUI BombText => bombText;
    }
}