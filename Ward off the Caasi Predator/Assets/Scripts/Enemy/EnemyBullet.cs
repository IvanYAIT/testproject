using Player;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Enemy
{
    public class EnemyBullet : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private float speed;
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] private LayerMask enviromentLayer;
        [SerializeField] private float lifeTime;
        private int _playerLayerMask;
        private int _enviromentLayerMask;

        private void Awake()
        {
            _playerLayerMask = (int)Mathf.Log(playerLayer.value, 2);
            _enviromentLayerMask = (int)Mathf.Log(enviromentLayer.value, 2);
            rb.AddForce(transform.up * speed, ForceMode2D.Impulse);
        }

        private void OnEnable()
        {
            StartCoroutine(LifeTime());
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == _playerLayerMask)
            {
                collision.GetComponent<Character>().GetDamage();
                gameObject.SetActive(false);
            }

            if (collision.gameObject.layer == _enviromentLayerMask)
                gameObject.SetActive(false);
        }

        private IEnumerator LifeTime()
        {
            yield return new WaitForSeconds(lifeTime);
            gameObject.SetActive(false);
        }
    }
}