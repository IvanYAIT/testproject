using UnityEngine;

namespace Level
{
    public class GoldenRoom : MonoBehaviour
    {
        [SerializeField] private Transform spawnPoint1;
        [SerializeField] private Transform spawnPoint2;
        [SerializeField] private Transform spawnPoint3;
        [SerializeField] private GameObject artifactPrefab;

        private ArtifactCharacteristic[] _artifactCharacteristics;
        private int _amountOfArtifacts;
        private void Awake()
        {
            _artifactCharacteristics = Resources.LoadAll<ArtifactCharacteristic>("");
            _amountOfArtifacts = Random.Range(1, 4);
            SpawnArtifact();
        }

        private void SpawnArtifact()
        {
            System.Random rnd = new System.Random();
            if (_amountOfArtifacts == 1)
            {
                GameObject obj = Instantiate(artifactPrefab, spawnPoint2.position, Quaternion.identity);
                obj.GetComponent<Artifact>().SetArtifactCharacteristic(_artifactCharacteristics[rnd.Next(0, _artifactCharacteristics.Length)]);
            } else if(_amountOfArtifacts == 2)
            {
                GameObject obj = Instantiate(artifactPrefab, spawnPoint1.position, Quaternion.identity);
                obj.GetComponent<Artifact>().SetArtifactCharacteristic(_artifactCharacteristics[rnd.Next(0, _artifactCharacteristics.Length)]);
                GameObject obj1 = Instantiate(artifactPrefab, spawnPoint3.position, Quaternion.identity);
                obj1.GetComponent<Artifact>().SetArtifactCharacteristic(_artifactCharacteristics[rnd.Next(0, _artifactCharacteristics.Length)]);
            } else if(_amountOfArtifacts == 3)
            {
                GameObject obj = Instantiate(artifactPrefab, spawnPoint1.position, Quaternion.identity);
                obj.GetComponent<Artifact>().SetArtifactCharacteristic(_artifactCharacteristics[rnd.Next(0, _artifactCharacteristics.Length)]);
                GameObject obj1 = Instantiate(artifactPrefab, spawnPoint2.position, Quaternion.identity);
                obj1.GetComponent<Artifact>().SetArtifactCharacteristic(_artifactCharacteristics[rnd.Next(0, _artifactCharacteristics.Length)]);
                GameObject obj2 = Instantiate(artifactPrefab, spawnPoint3.position, Quaternion.identity);
                obj2.GetComponent<Artifact>().SetArtifactCharacteristic(_artifactCharacteristics[rnd.Next(0, _artifactCharacteristics.Length)]);
            }
        }
    }
}