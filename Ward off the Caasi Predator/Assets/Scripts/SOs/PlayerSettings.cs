using UnityEngine;

namespace Player
{
    public class PlayerSettings : MonoBehaviour
    {
        [SerializeField] private Transform heartsParent;
        [SerializeField] private Rigidbody2D playerRb;
        [SerializeField] private Transform playerHead;

        public Transform HeartsParent => heartsParent;
        public Transform PlayerHead => playerHead;
        public Rigidbody2D PlayerRb => playerRb;
    }
}