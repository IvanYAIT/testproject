using Level;
using NavMeshPlus.Components;
using Player;
using UnityEngine;
using Zenject;

namespace Core
{
    public class MainInstaller : MonoInstaller
    {
        [Header("Player")]
        [SerializeField] private PlayerCharacteristics playerCharacteristics;
        [SerializeField] private PlayerSettings playerSettings;
        [SerializeField] private HealthSystem healthSystem;
        [SerializeField] private Transform playerTransform;
        [SerializeField] private Character character;
        [SerializeField] private InputListener inputListener;
        [Header("Bullet")]
        [SerializeField] private BulletSettings bulletSettings;
        [Header("Bomb")]
        [SerializeField] private BombSettings bombSettings;
        [SerializeField] private BombSystem bombSystem;
        [SerializeField] private BombView bombView;
        [Header("Level")]
        [SerializeField] private NavMeshSurface surface2D;
        [SerializeField] private LevelSettings levelSettings;


        public override void InstallBindings()
        {
            Container.Bind<Game>().AsSingle().NonLazy();

            Container.Bind<BulletSettings>().FromInstance(bulletSettings).AsSingle().NonLazy();
            Container.Bind<BulletPool>().AsSingle().NonLazy();

            Container.Bind<NavMeshSurface>().FromInstance(surface2D).AsSingle().NonLazy();
            Container.Bind<LevelSettings>().FromInstance(levelSettings).AsSingle().NonLazy();
            Container.Bind<SmartLevelGenerator>().AsSingle().NonLazy();

            Container.Bind<PlayerController>().AsSingle().NonLazy();
            Container.Bind<PlayerSettings>().FromInstance(playerSettings).AsSingle().NonLazy();
            Container.Bind<PlayerCharacteristics>().FromInstance(playerCharacteristics).AsSingle().NonLazy();
            Container.Bind<Transform>().FromInstance(playerTransform).AsSingle().NonLazy();
            Container.Bind<HealthSystem>().AsSingle().NonLazy();

            Container.Bind<BombView>().FromInstance(bombView).AsSingle().NonLazy();
            Container.Bind<BombSettings>().FromInstance(bombSettings).AsSingle().NonLazy();
            Container.Bind<BombSystem>().FromInstance(bombSystem).AsSingle().NonLazy();

            Container.Bind<InputListener>().FromInstance(inputListener).AsSingle().NonLazy();

            Container.BindFactory<Bullet, BulletPool.BulletFactory>().FromComponentInNewPrefab(bulletSettings.BulletPrefab).AsSingle();
        }
    }
}