using UnityEngine;

namespace Player
{
    [CreateAssetMenu(fileName = "BulletSettings", menuName = "SO/NewBulletSettings")]
    public class BulletSettings : ScriptableObject
    {
        [SerializeField] private int bulletAmount;
        [SerializeField] private GameObject bulletPrefab;

        public int BulletAmount => bulletAmount;
        public GameObject BulletPrefab => bulletPrefab;
    }
}