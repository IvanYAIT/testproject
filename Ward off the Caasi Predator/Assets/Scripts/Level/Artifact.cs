using UnityEngine;

namespace Level
{
    public class Artifact : MonoBehaviour
    {
        [SerializeField] private LayerMask playerLayerMask;

        public ArtifactCharacteristic ArtifactCharacteristic { get; private set; }

        private ArtifactView artifactView;
        private int _playerLayer;

        private void Awake()
        {
            GameObject.FindGameObjectWithTag("View").TryGetComponent<ArtifactView>(out artifactView);
            _playerLayer = (int)Mathf.Log(playerLayerMask.value, 2);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == _playerLayer)
            {
                artifactView.ArtifactPanel.SetActive(true);
                artifactView.ArtifactNameText.text = ArtifactCharacteristic.Name;
                artifactView.HealthText.text = $"HP: {ArtifactCharacteristic.Health}";
                artifactView.DamageText.text = $"DPS: {ArtifactCharacteristic.Damage}";
                artifactView.BulletSpeedText.text = $"BulletSpeed: {ArtifactCharacteristic.BulletSpeed}";
                artifactView.PlayerSpeedText.text = $"PlayerSpeed: {ArtifactCharacteristic.PlayerSpeed}";
                artifactView.ShootSpeedText.text = $"ShootCooldown: {ArtifactCharacteristic.ShootCooldown}";

            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.layer == _playerLayer)
            {
                artifactView.ArtifactPanel.SetActive(false);

            }
        }

        public void SetArtifactCharacteristic(ArtifactCharacteristic artifactCharacteristic) =>
            ArtifactCharacteristic = artifactCharacteristic;
    }
}