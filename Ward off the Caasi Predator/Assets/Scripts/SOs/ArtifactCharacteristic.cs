using UnityEngine;

namespace Level
{
    [CreateAssetMenu(fileName = "ArtifactCharacteristic", menuName = "SO/NewArtifactCharacteristic")]
    public class ArtifactCharacteristic : ScriptableObject
    {
        [SerializeField] private string name;
        [SerializeField] private int health;
        [SerializeField] private float damage;
        [SerializeField] private float playerSpeed;
        [SerializeField] private float shootCooldown;
        [SerializeField] private float bulletSpeed;

        public string Name => name;
        public int Health => health;
        public float Damage => damage;
        public float PlayerSpeed => playerSpeed;
        public float ShootCooldown => shootCooldown;
        public float BulletSpeed => bulletSpeed;
    }
}