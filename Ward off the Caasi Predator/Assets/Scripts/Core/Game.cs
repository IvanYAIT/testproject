using System;
using UnityEngine.SceneManagement;

public class Game
{
    public static Action OnEnd;
    public static Action OnWin;

    public Game()
    {
        OnEnd += End;
        OnWin += Win;
    }

    public void End()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        OnEnd -= End;
    }

    public void Win()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        OnWin -= Win;
    }
}
