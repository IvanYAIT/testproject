using UnityEngine;

namespace Level
{
    [CreateAssetMenu(fileName = "LevelSettings", menuName = "SO/NewLevelSettings")]
    public class LevelSettings : ScriptableObject
    {
        [SerializeField] private int minAmountOfRooms;
        [SerializeField] private int maxAmountOfRooms;
        [SerializeField] private GameObject[] roomPrefabs;
        [SerializeField] private GameObject startRoomPrefab;
        [SerializeField] private GameObject bossRoomPrefab;
        [SerializeField] private GameObject goldenRoomPrefab;

        public int MinAmountOfRooms => minAmountOfRooms;
        public int MaxAmountOfRooms => maxAmountOfRooms;
        public GameObject[] RoomPrefabs => roomPrefabs;
        public GameObject StartRoomPrefab => startRoomPrefab;
        public GameObject BossRoomPrefab => bossRoomPrefab;
        public GameObject GoldenRoomPrefab => goldenRoomPrefab;
    }
}