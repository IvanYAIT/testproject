using Level;
using UnityEngine;
using Zenject;

namespace Player
{
    public class Character : MonoBehaviour
    {
        [SerializeField] private LayerMask doorLayerMask;
        [SerializeField] private LayerMask heartLayerMask;
        [SerializeField] private LayerMask bombLayerMask;
        [SerializeField] private LayerMask artifactLayerMask;

        private int _doorLayer;
        private HealthSystem _healthSystem;
        private BombSystem _bombSystem;
        private int _heartLayer;
        private int _bombLayer;
        private int _artifactLayer;
        private InputListener _inputListener;

        private void Start()
        {
            _doorLayer = (int)Mathf.Log(doorLayerMask.value, 2);
            _heartLayer = (int)Mathf.Log(heartLayerMask.value, 2);
            _bombLayer = (int)Mathf.Log(bombLayerMask.value, 2);
            _artifactLayer = (int)Mathf.Log(artifactLayerMask.value, 2);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == _doorLayer)
            {
                switch (collision.gameObject.tag.ToLower())
                {
                    case "left":
                        transform.position = new Vector2(transform.position.x - 3, transform.position.y);
                        break;
                    case "right":
                        transform.position = new Vector2(transform.position.x + 3, transform.position.y);
                        break;
                    case "up":
                        transform.position = new Vector2(transform.position.x, transform.position.y + 4);
                        break;
                    case "down":
                        transform.position = new Vector2(transform.position.x, transform.position.y - 4);
                        break;
                }
            }

            if (collision.gameObject.layer == _heartLayer)
            {
                if(_healthSystem.HealHeart())
                    collision.gameObject.SetActive(false);
            }
            if (collision.gameObject.layer == _bombLayer)
            {
                if(_bombSystem.AddBomb())
                    collision.gameObject.SetActive(false);
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer == _artifactLayer)
            {
                Artifact artifact;
                collision.gameObject.TryGetComponent<Artifact>(out artifact);
                collision.gameObject.SetActive(false);
                ChangeStats(artifact.ArtifactCharacteristic);
            }
        }

        private void ChangeStats(ArtifactCharacteristic characteristic)
        {
            if (characteristic.Health < 0)
                for (int i = 0; i < characteristic.Health * -1; i++)
                {
                    _healthSystem.DecreaseMaxHealth();
                }
            else if (characteristic.Health > 0)
                for (int i = 0; i < characteristic.Health; i++)
                {
                    _healthSystem.IncreaseMaxHealth();
                }

            _inputListener.ChangeStats(characteristic);
        }

        [Inject]
        public void Construct(HealthSystem healthSystem, BombSystem bombSystem, InputListener inputListener)
        {
            _healthSystem = healthSystem;
            _bombSystem = bombSystem;
            _inputListener = inputListener;
        }

        public void GetDamage()
        {
            _healthSystem.GetDamage();
        }
    }
}