using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Player
{
    public class BulletPool
    {
        public List<GameObject> Bullets { get; private set; }
        private BulletFactory _factory;

        [Inject]
        public BulletPool(BulletSettings bulletSettings, BulletFactory factory)
        {
            Bullets = new List<GameObject>();
            _factory = factory;
            InitPool(bulletSettings.BulletAmount);
        }

        private void InitPool(int poolSize)
        {
            for (int i = 0; i < poolSize; i++)
            {
                Bullet currentBullet = _factory.Create();
                Bullets.Add(currentBullet.gameObject);
            }
        }

        public GameObject Get(int index) => Bullets[index];

        public void Return()
        {
            Bullet currentBullet = _factory.Create();
            Bullets.Add(currentBullet.gameObject);
        }

        public class BulletFactory : PlaceholderFactory<Bullet> { }
    }
}