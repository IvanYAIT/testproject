using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Enemy
{
    public class RangedEnemy : MonoBehaviour, IEnemy
    {
        [SerializeField] private float hp;
        [SerializeField] private float startShootingRadius;
        [SerializeField] private float cooldown;
        [Space]
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private Transform shootPoint;
        [SerializeField] private Transform gun;

        private int playerLayerMask;
        private Rigidbody2D _rb;
        private float _currentHp;
        private NavMeshAgent _agent;
        private bool _onCooldown;
        private Transform _player;

        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player").transform;
            _currentHp = hp;
            _rb = GetComponent<Rigidbody2D>();
            playerLayerMask = (int)Mathf.Log(playerLayer.value, 2);
        }

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _agent.updateRotation = false;
            _agent.updateUpAxis = false;
        }

        void Update()
        {
            _rb.velocity = new Vector2();

            float distanceToPlayer = Vector2.Distance(transform.position, _player.position);
            
            if (distanceToPlayer <= startShootingRadius)
            {
                Rotate();
                _agent.ResetPath();
                if (!_onCooldown)
                    StartCoroutine(Attack());
                    
            }
            else
            {
                Rotate();
                _agent.SetDestination(_player.transform.position);
            }
        }

        private IEnumerator Attack()
        {
            Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
            _onCooldown = true;
            yield return new WaitForSeconds(cooldown);
            _onCooldown = false;
        }

        public void GetDamage(float damage)
        {
            _currentHp -= damage;
            CheckHealth();
        }

        private void CheckHealth()
        {
            if (_currentHp <= 0)
                Death();
        }

        public void Rotate()
        {
            Vector2 aimDirection = (Vector2)_player.position - _rb.position;
            float aimAngle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg - 90f;
            gun.rotation = Quaternion.AngleAxis(aimAngle, new Vector3(0, 0, 1));
        }

        public void Death()
        {
            Destroy(gameObject);
        }
    }
}