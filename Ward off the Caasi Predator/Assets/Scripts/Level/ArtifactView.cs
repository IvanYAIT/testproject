using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Level
{
    public class ArtifactView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI healthText;
        [SerializeField] private TextMeshProUGUI damageText;
        [SerializeField] private TextMeshProUGUI playerSpeedText;
        [SerializeField] private TextMeshProUGUI shootSpeedText;
        [SerializeField] private TextMeshProUGUI bulletSpeedText;
        [SerializeField] private TextMeshProUGUI artifactNameText;
        [SerializeField] private GameObject artifactPanel;

        public GameObject ArtifactPanel => artifactPanel;
        public TextMeshProUGUI ArtifactNameText => artifactNameText;
        public TextMeshProUGUI HealthText => healthText;
        public TextMeshProUGUI DamageText => damageText;
        public TextMeshProUGUI PlayerSpeedText => playerSpeedText;
        public TextMeshProUGUI ShootSpeedText => shootSpeedText;
        public TextMeshProUGUI BulletSpeedText => bulletSpeedText;
    }
}