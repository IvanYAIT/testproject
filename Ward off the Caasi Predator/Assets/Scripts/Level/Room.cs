using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Level
{
    public class Room : MonoBehaviour
    {
        [SerializeField] private LayerMask playerLayerMask;
        [SerializeField] private LayerMask bossRoomLayerMask;
        [SerializeField] private LayerMask goldenRoomLayerMask;
        [SerializeField] private LayerMask roomLayerMask;
        [SerializeField] private Transform doorsParent;
        [SerializeField] private Transform enemyParent;
        [SerializeField] private bool isSpecial;
        [SerializeField] private bool isStartedRoom;
        [SerializeField] private GameObject[] pickableObjects;
        [SerializeField] private Transform pickableObjectSpawnPoint;

        public int Index { get; private set; }

        private int[] _doors;
        private int _bossRoomLayer;
        private int _goldenRoomLayer;
        private int _roomLayer;
        private int _playerLayer;
        private bool isDoorsSet;
        private bool isPickableObjectSpawn;

        private void Start()
        {
            _bossRoomLayer = (int)Mathf.Log(bossRoomLayerMask.value, 2);
            _goldenRoomLayer = (int)Mathf.Log(goldenRoomLayerMask.value, 2);
            _playerLayer = (int)Mathf.Log(playerLayerMask.value, 2);
            _roomLayer = (int)Mathf.Log(roomLayerMask.value, 2);
            if (isStartedRoom)
                isPickableObjectSpawn = true;
        }

        private void Update()
        {
            if (enemyParent.childCount <= 0 && isDoorsSet)
            {
                CreateDoors();
                if (!isSpecial && !isPickableObjectSpawn)
                {
                    int rnd = Random.Range(0, 3);
                    if(rnd < 2)
                        Instantiate(pickableObjects[rnd], pickableObjectSpawnPoint.position, Quaternion.identity);
                    isPickableObjectSpawn = true;
                }
            }

        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == _playerLayer)
            {
                CameraMover.OnMove?.Invoke(new Vector3(transform.position.x, transform.position.y, -10));
                for (int i = 0; i < enemyParent.childCount; i++)
                {
                    enemyParent.GetChild(i).gameObject.SetActive(true);
                }
            }

        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.layer == _bossRoomLayer || collision.gameObject.layer == _goldenRoomLayer)
                gameObject.SetActive(false);
            if (collision.gameObject.layer == _roomLayer && isStartedRoom)
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void Construct(int index)
        {
            Index = index;
        }

        public void SetDoors(int[] doors)
        {
            _doors = doors;
            isDoorsSet = true;
        }

        private void CreateDoors()
        {
            if (_doors[0] == 1)
                doorsParent.GetChild(0).gameObject.SetActive(true);
            if (_doors[1] == 1)
                doorsParent.GetChild(1).gameObject.SetActive(true);
            if (_doors[2] == 1)
                doorsParent.GetChild(2).gameObject.SetActive(true);
            if (_doors[3] == 1)
                doorsParent.GetChild(3).gameObject.SetActive(true);
        }
    }
}