using Level;

using UnityEngine;
using Zenject;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {

        private SmartLevelGenerator _smartLevelGenerator;

        void Start()
        {
            Game game = new Game();
        }

        private void Update()
        {
            _smartLevelGenerator?.Generate();
        }

        [Inject]
        public void Construct(SmartLevelGenerator smartLevelGenerator)
        {
            _smartLevelGenerator = smartLevelGenerator;
        }
    }
}