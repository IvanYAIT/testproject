using UnityEngine;
using Zenject;

namespace Player
{
    public class BombSystem : MonoBehaviour
    {
        private const int MAX_AMOUNT_OF_BOMBS = 99;

        private Transform _player;
        private GameObject _bombPrefab;
        private BombView _bombView;
        private int _currentAmountBomb;

        [Inject]
        public void Construct(Transform player, BombSettings bombSettings, BombView bombView)
        {
            _bombView = bombView;
            _player = player;
            _bombPrefab = bombSettings.BombPrefab;
            _currentAmountBomb = bombSettings.BombCount;
            UpadateView();
        }

        public void UseBomb()
        {
            if (_currentAmountBomb > 0)
            {
                GameObject obj = Instantiate(_bombPrefab, _player.position, Quaternion.identity);
                _currentAmountBomb--;
                UpadateView();
            }
        }

        public bool AddBomb()
        {
            if(_currentAmountBomb <= MAX_AMOUNT_OF_BOMBS)
            {
                _currentAmountBomb++;
                UpadateView();
                return true;
            }
            return false;
        }

        private void UpadateView() =>
            _bombView.BombText.text = $"{_currentAmountBomb}";
    }
}