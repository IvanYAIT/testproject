using UnityEngine;

namespace Player
{
    [CreateAssetMenu(fileName = "BombSettings", menuName = "SO/NewBombSettings")]
    public class BombSettings : ScriptableObject
    {
        [SerializeField] private int bombCount;
        [SerializeField] private GameObject bombPrefab;

        public int BombCount => bombCount;
        public GameObject BombPrefab => bombPrefab;
    }
}