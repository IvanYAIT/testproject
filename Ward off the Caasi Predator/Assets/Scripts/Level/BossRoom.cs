using UnityEngine;

namespace Level
{
    public class BossRoom : MonoBehaviour
    {
        [SerializeField] private GameObject enemyParent;

        private void Update()
        {
            if (enemyParent.transform.childCount == 0)
                Game.OnWin?.Invoke();
        }
    }
}